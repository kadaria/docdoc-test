var task = new Select();

window.onload = function() {
	task.updateText(task.input.value);
};

task.input.onchange = function (event) {
	task.updateText(task.input.value);
};


function Select() {
	var select = this;
	var __cache = {};
	var load = "Загрузка...";
	var error = "Произошла ошибка";

	this.input = document.getElementById('input');
	this.output = document.getElementById('output');

	function getFile(url) {
		return new Promise(function(resolve, reject) {
			var xhr = new XMLHttpRequest();
			xhr.open('GET', url, true);

			xhr.onload = function() {
				if (this.readyState != 4) return;	
				if (this.status == 200 && resolve) {
					resolve(this.responseText);
				} else {
					reject();
				}
			};

			xhr.send();
		});
	}

	function getContent(type,content) {
		var format = type.split('.').pop().toLowerCase();
		var parser = {
			js : function(data, url) {
				var script = document.createElement('script');
				script.src = url;
				document.body.appendChild(script);

				window.cb = function(object) {
					__cache[url] = object.text;
					sendContent(object.text, url);
					document.body.removeChild(script);
					delete window.cb;
				};

				return '';
			},
			json : function(data) {
				var result = !data ? error : JSON.parse(data);
				return result.text;
			},
			txt : function(data) {
				return data;
			}
		}
		try {
			return parser[format](content, type);
		} catch(e) {
			return e;
		};
	}

	function sendContent(result, url) {
		if (result.length == 0) {
			return;
		}
		__cache[url] = result;
		select.output.value = result;
	}

	this.updateText = function updateText(url) {
		if (__cache[url]) {
			return sendContent(__cache[url]);
		}

		sendContent(load);

		var cachePromise = getFile(url);

		cachePromise.then(function(response) {
			var result = getContent(url,response);
			sendContent(result, url);
		});

		cachePromise.catch(function(){
			sendContent(error);
		});
	}
}

